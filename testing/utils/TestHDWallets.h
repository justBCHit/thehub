/*
 * This file is part of the Flowee project
 * Copyright (C) 2018-2022 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TEST_APPUTILS_H
#define TEST_APPUTILS_H

#include <common/TestFloweeBase.h>

#include <primitives/pubkey.h>

class TestHDWallets : public TestFloweeBase
{
    Q_OBJECT
public:
    TestHDWallets();
    ~TestHDWallets();

private slots:
    // bip32
    void hdwallet();
    void hdwallet_data();

private:
    ECCVerifyHandle globalVerifyHandle;
};

#endif
