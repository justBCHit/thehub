/*
 * This file is part of the Flowee project
 * Copyright (C) 2021 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "CleanPeersAction.h"
#include "DownloadManager.h"
#include "Peer.h"

#include <Logger.h>

CleanPeersAction::CleanPeersAction(DownloadManager *parent)
    : Action(parent)
{
    setInterval(35 * 1000);
}

void CleanPeersAction::execute(const boost::system::error_code &error)
{
    if (error)
        return;

    if (std::abs(m_dlm->blockchain().expectedBlockHeight() - m_dlm->blockHeight()) > 4000) {
        // don't do anything until we got at least close enough to have useful 'HEADER' calls.
        again();
        return;
    }

    for (auto peer : m_dlm->connectionManager().connectedPeers()) {
        logDebug() << "peer" << peer->connectionId() << "headers" << peer->peerAddress().lastReceivedGoodHeaders()
                   << time(nullptr) - peer->peerAddress().lastConnected() << "s";
        if (!peer->receivedHeaders() && peer->peerAddress().lastReceivedGoodHeaders() == 0) {
            // make sure that we actually request headers from peers.
            if (!peer->requestedHeader()) {
                m_dlm->connectionManager().requestHeaders(peer);
            }
            else if (time(nullptr) - peer->peerAddress().lastConnected() > 90) {
                // ban peers that never responded to our request for headers
                logWarning() << "CleanPeersAction disconnects peer"
                             << peer->connectionId() << "that has not responded to 'HEADERS' call for 90 seconds";

                m_dlm->connectionManager().punish(peer, PUNISHMENT_MAX);
            }
        }
    }

    again();
}
