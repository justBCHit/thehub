/*
 * This file is part of the Flowee project
 * Copyright (C) 2021 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MNEMONIC_H
#define MNEMONIC_H

#include <QMap>
#include <QStringList>
#include <vector>

/**
 * Supplies validation of the BIP39 style mnemonics
 *
 * Notice that other than validation this class is not going to be helpful to
 * process the actual data, check out the class HDMasterKey for that.
 *
 * A single Mnemonic instance holds a list of possible dictionaries and allows
 * UI validation of the text using findWord() an clearSelectedLanguage().
 * If it looks like the user is done with the whole 12 or more words mnemonic
 * you can use the validateMnemonic() call. When this returns `Valid` we know that
 * the mnemonic sentence is proper and checksums pass.
 */
class Mnemonic
{
public:
    /**
     * Used by validateMnemonic()
     */
    enum Validity {
        Valid,              ///< A fully valid mnemonic was found.
        WhitespaceError,    ///< please fix your whitespace before validating.
        IncorrectWordCount, ///< failed basic checks
        UnknownLanguage,    ///< first word can not be found.
        UnknownWord,        ///< word can not be found in selected dict
        ChecksumFailure     ///< Incorrect checksum
    };

    /**
     * Find a word in the dictionary, or a dictionary to fit the word.
     * This method will fill the \a dict on first use and fetch the
     * index on further usages from the existng dict.
     * @return the word, or -1 if not found.
     */
    int findWord(const QString &word);

    /// returns the selected id of the wordlist. Or empty if nothing is selected.
    QString languageId() const;
    void clearSelectedLanguage();

    /// Add a word-list and languageId.
    void registerWordList(const QString &id, const QString &filename);

    /**
     * Validate the mnemonic string on correctness.
     * This method will check the individual words actually match
     * a known word-list and also check the checksum build into the bip32-style
     * mnemonics.
     * @param mnemonic the actual full sentence of words.
     * @param[out] index-in-text of the first word not recognized, if any.
     */
    Validity validateMnemonic(const QString &mnemonic, int &errorIndex);

    QString generateMnemonic(const std::vector<uint8_t> &entropy, const QString &langId = QString("en")) const;

private:
    QMap<QString, QString> m_wordLists; // id -> filename

    QString m_languageId;
    QStringList m_words;
};

#endif
